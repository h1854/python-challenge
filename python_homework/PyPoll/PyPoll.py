# You will be give a set of poll data called election_data.csv. The dataset is composed of three columns: Voter ID, County, and Candidate. Your task is to create a Python script that analyzes the votes and calculates each of the following:

# 1. The total number of votes cast
# 2. A complete list of candidates who received votes
# 3. The percentage of votes each candidate won
# 4. The total number of votes each candidate won
# 5. The winner of the election based on popular vote.

import csv
TotalVotes = 0
KhanVotes = 0
CorreyVotes = 0
LiVotes = 0
OTooleyVotes = 0
with open('election_data.csv','r') as csv_file:
    csv_reader = csv.reader(csv_file)
    next(csv_reader)
    for line in csv_reader:
        TotalVotes = TotalVotes + 1

        if line[2] == "Khan":
            KhanVotes = KhanVotes + 1
        elif line[2] == "Correy":
            CorreyVotes = CorreyVotes + 1
        elif line[2] == "Li":
            LiVotes = LiVotes + 1
        elif line[2] == "O'Tooley":
            OTooleyVotes = OTooleyVotes + 1


# Percentage
KhanPercent = (KhanVotes/TotalVotes) * 100
CorreyPercent = (CorreyVotes/TotalVotes) * 100
LiPercent = (LiVotes/TotalVotes) * 100
OTooleyPercent = (OTooleyVotes/TotalVotes) * 100

# Creating a dictionary

Results = {
    "Khan" : KhanVotes,
    "Correy" : CorreyVotes,
    "Li" : LiVotes,
    "O'Tooley" : OTooleyVotes
}

# Determining winner
winner = max(Results,key=Results.get)

output = "Total Votes: {}\n\n" \
         "Khan: {:.2f}% ({})\n" \
         "Correy: {:.2f}% ({})\n" \
         "Li: {:.2f}% ({})\n" \
         "O'Tooley: {:.2f}% ({})\n\n" \
         "Winner: {} ".format(TotalVotes,KhanPercent,KhanVotes,CorreyPercent,CorreyVotes,LiPercent,LiVotes,OTooleyPercent,OTooleyVotes,winner)


print(output)

# Exporting to text file
import sys
sys.stdout = open("PyPoll.txt","w") # This creates a new text file w means overwrite, can replace with "a" to append
print(output) # Prints to above text file
sys.stdout.close() # Closes