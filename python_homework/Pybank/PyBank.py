# Your task is to create a Python script that analyzes the records to calculate each of the following:
# 1. The total number of months included in the dataset
# 2. The net total amount of "Profit/Losses" over the entire period
# 3. Calculate the changes in "Profit/Losses" over the entire period, then find the average of those changes
# 4. The greatest increase in profits (date and amount) over the entire period
# 5. The greatest decrease in profits (date and amount) over the entire period

import csv
# Initializing variables that will be used for calculations
count = 0
NetTotal = 0
ProfitLoss = []
MonthDate = []

with open('budget_data.csv','r') as csv_file:
    csv_reader = csv.reader(csv_file)
    next(csv_reader) # This next function skips the headers - Headers are not part of the dataset..

    for line in csv_reader:
        # print(line) #Uncomment to view csv file
        count = count + 1
        ProfitLoss.append(line[1]) # This populates the ProfitLoss array with a profit loss (STRING FORMAT)
        MonthDate.append(line[0]) # This populates the MonthDate array with dates (STRING FORMAT)


# Converting ProfitLoss array into integers in order to perform calculations
ProfitLossInt = []
for i in ProfitLoss:
    ProfitLossInt.append(int(i))

# Net total profit & losses
NetTotal = sum(ProfitLossInt)

# Calculating Profit/loss change per month
PLChange = []
for j in range(len(ProfitLossInt)-1): # We need a to -1 from the length to avoid the index from exceeding the array max index, j takes on the value of 0 to the length of array - 1
    PLChange.append(ProfitLossInt[j+1] - ProfitLossInt[j]) # We want to take "next month - current month"

# Average
avg = sum(PLChange) / len(PLChange)

# Greatest decrease in profits (Date & Amount)
MaxProfit = max(PLChange)
MaxProfitIndex = PLChange.index(MaxProfit)
MaxProfitMonth = MonthDate[MaxProfitIndex + 1] # We need to plus 1 here because change between jan to feb is essentially feb while MonthDate list starts from Jan

# Greatest decrease in profits (Date & Amount)
MinProfit = min(PLChange)
MinProfitIndex = PLChange.index(MinProfit)
MinProfitMonth = MonthDate[MinProfitIndex + 1] # We need to plus 1 here because change between jan to feb is essentially feb while MonthDate list starts from Jan


output = "1. Total Number of months: {} \n" \
         "2. Net Total Profit/Losses: ${}\n" \
         "3. Average of CHANGES in profit/losses: ${}\n" \
         "4. Greatest Increase in profits was ${} which occurred in {}\n" \
         "5. Greatest Decrease in profits was ${} which occurred in {}".format(count,NetTotal,avg,MaxProfit,MaxProfitMonth,MinProfit,MinProfitMonth)

print(output)

# print("1. Total Number of months: {}".format(count))
# print("2. Net total profit/losses: ${}".format(NetTotal))
# print("3. Average of CHANGES in profit/loss: ${}".format(avg))
# print("4. Greatest Increase in profits was ${} which occured in {}".format(MaxProfit,MaxProfitMonth))
# print("5. Greatest Decrease in profits was ${} which occured in {}".format(MinProfit,MinProfitMonth))


# Exporting to text file
import sys
sys.stdout = open("PyBank.txt","w") # This creates a new text file w means overwrite, can replace with "a" to append
print(output) # Prints to above text file
sys.stdout.close() # Closes











